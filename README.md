# Acquia CI

## What is this?

This file provides a [RoboFile](https://robo.li/) that allows doing Acquia 
cloud tasks.

## How do I use it?
You should be able to use it by:
> vendor/bin/robo --load-from vendor/bixal/acquia-ci/AcquiaCiCommands.phpRoboFile.php
This should show you all the tasks available

One also needs to create an acquia-ci.yml file in their root directory.

    acquia:
      subName: [Subscription Name]
      appUuid: [Application UUID]
      envMap:
        [Drush Alias with the @]: [Environment UUID]
        [Drush Alias with the @]: [Environment UUID]..
      prodEnv: [Drush Alias with the @, usually prod]
      gitUrl: [Acquia Git URL]
      dbName: [Database Name]

In addition, you need to create an API user and token and provide robo with these as an environment variable or in a .env file.

    AWS_ACCESS_KEY_ID=[Your user]
    AWS_SECRET_ACCESS_KEY=[Your token]


## Example of backing up and reverting the DB in a single pipeline

Install composer dependencies (robo and drush)
> composer install --ignore-platform-reqs

Copy DB from prod to a dev2
> robo acquia:copy-db dev2 prod

Make a change to the DB on dev 2  
> drush @dev2 state-get system.maintenance_mode
 
Print out the change  
> robo drush:maintenance-mode dev2
  
Backup dev2 to a DB backup file
> robo acquia:db-backup-create dev2)

Retrieve the last backup ID created
> BACKUP_ID=$(./robo.sh acquia:db-backups-last dev2)

Print out the backup ID
> echo $BACKUP_ID

Copy production to dev2 to overwrite our DB update
> robo acquia:copy-db dev2 prod

Print out change we made before the DB was wiped out, it should 0
> drush @dev2 state-get system.maintenance_mode

Import the backup DB
> robo acquia:db-backup-restore dev2 $BACKUP_ID

Print out the change we made before the DB was wiped out, it should be 1
> drush @dev2 state-get system.maintenance_mode