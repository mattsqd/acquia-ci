<?php

// @codingStandardsIgnoreStart

use Consolidation\OutputFormatters\StructuredData\UnstructuredData;
use Consolidation\OutputFormatters\StructuredData\UnstructuredListData;
use Lullabot\RoboAcquia\AcquiaTaskWatcher;
use Robo\Exception\TaskExitException;
use Robo\Result;
use Robo\ResultData;
use Robo\Tasks;
use Lullabot\RoboAcquia\LoadRoboAcquiaTasks;

/**
 * Commands used to interact with the WIOA CI environment.
 *
 * This file must be in the root namespace.
 *
 * @class RoboFile
 * @codeCoverageIgnore
 */
class AcquiaCiCommands extends Tasks
{

  // Import the Lullabot Acquia Task.
  use LoadRoboAcquiaTasks;

  /**
   * AcquiaTaskSwitcher code for command copyFiles finishing.
   */
  protected const TASK_CODE_FILES_COPIED = 'FilesCopied';

  /**
   * AcquiaTaskSwitcher code for command deployCode finishing.
   */
  protected const TASK_CODE_CODE_DEPLOYED = 'CodeDeployed';

  /**
   * Running in CI environment if true.
   *
   * @var bool
   */
  protected $is_ci = FALSE;

  /**
   * The acquia subscription name.
   *
   * @var string
   */
  protected $acquiaSubName;

  /**
   * The acquia Application UUID.
   *
   * @var string
   */
  protected $acquiaApplicationUuid;

  /**
   * The drush alias to env UUID map.
   *
   * @var string
   */
  protected $acquiaEnvMap;

  /**
   * The acquia Git URL.
   *
   * @var string
   */
  protected $acquiaGitURL;

  /**
   * The acquia DB Name.
   *
   * @var string
   */
  protected $acquiaDbName;

  /**
   * The prod acquia alias.
   *
   * @var string
   */
  protected $acquiaProdEnv;

  /**
   * The acquia API User.
   *
   * @var string
   */
  protected $acquiaKey;

  /**
   * The acquia API Token.
   *
   * @var string
   */
  protected $acquiaSecret;

  /**
   * RoboFile constructor.
   */
  public function __construct() {
    // The commands in here expect to be run from the root directory.
    $reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
    chdir(dirname(dirname(dirname($reflection->getFileName()))));
    // Include acquia-ci.yml as a config file.
    \Robo\Robo::loadConfiguration(['acquia-ci.yml']);
    $acquiaConfig = \Robo\Robo::Config()->get('acquia');
    $this->acquiaSubName = $acquiaConfig['subName'] ?? '';
    $this->acquiaApplicationUuid = $acquiaConfig['appUuid'] ?? '';
    $this->acquiaEnvMap = $acquiaConfig['envMap'] ?? [];
    $this->acquiaGitURL = $acquiaConfig['gitUrl'] ?? '';
    $this->acquiaDbName = $acquiaConfig['dbName'] ?? '';
    $this->acquiaProdEnv = $acquiaConfig['prodEnv'] ?? 'prod';
    $env = [];
    if (file_exists('.env')) {
      $env = parse_ini_file('.env', FALSE, INI_SCANNER_RAW);
    }
    // Get the environment file from environment variables first and if not
    // set, use the .env file.
    $getEnv = static function($key) use ($env) {
      return getenv($key) ?: ($env[$key] ?? '');
    };
    $this->acquiaKey = $getEnv('ACQUIA_CLOUD_API_USER');
    $this->acquiaSecret = $getEnv('ACQUIA_CLOUD_API_TOKEN');
    if (getenv('CI')) {
      $this->is_ci = TRUE;
    }
  }

  /**
   * Turn an Acquia alias into an environment UUID.
   *
   * @param $alias
   *
   * @return string
   */
  protected function acquiaAliasToUUID($alias) {
    if (empty($this->acquiaEnvMap)) {
      throw new \RuntimeException('The acquia.envMap is required.');
    }
    if (!empty($this->acquiaEnvMap[$alias])) {
      return $this->acquiaEnvMap[$alias];
    }
    throw new \RuntimeException(
      sprintf(
        'An Acquia alias is required: %s.',
        implode(' or ', array_keys($this->acquiaEnvMap))
      )
    );
  }

  /**
   * Ensure the Acquia environment variables are set.
   */
  protected function acquiaReady() {
    $ready = TRUE;
    if (empty($this->acquiaKey)) {
      $this->yell('ACQUIA_CLOUD_API_USER is required, it should be the Acquia key');
      $ready = FALSE;
    }
    if (empty($this->acquiaSecret)) {
      $this->yell('ACQUIA_CLOUD_API_TOKEN is required, it should be the Acquia secret');
      $ready = FALSE;
    }
    if (FALSE === $ready) {
      throw new \RuntimeException('All Acquia Environment variables are not set.');
    }
  }

  /**
   * List out tasks.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredListData
   *
   * @command acquia:task-list
   */
  public function acquiaTaskList()
  {
    $this->acquiaReady();
    $response = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->tasks($this->acquiaApplicationUuid)
      ->run();
    $tasks = [];
    foreach ($response['result'] as $task) {
      /* @var \AcquiaCloudApi\Response\TaskResponse $task */
      $tasks[$task->uuid] = sprintf('%s: %s', $task->name, $task->status);
    }
    return new UnstructuredListData($tasks);
  }

  /**
   * List all backups for $alias.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredListData
   *
   * @command acquia:db-backups-info
   *
   * @param string $alias
   *   An Acquia alias.
   * @param string $type
   *   (Optional) Either daily or ondemand.
   */
  public function acquiaDatabaseBackupsInfo(string $alias, string $type = '')
  {
    $this->acquiaReady();
    $task = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->databaseBackups($this->acquiaAliasToUUID($alias), $this->acquiaDbName);
    if ('' !== $type) {
      $task->addQuery('filter', "type=$type");
    }
    $response = $task->run();
    $backups = [];
    foreach ($response['result'] as $backup) {
      /* @var \AcquiaCloudApi\Response\BackupResponse $backup */
      $backups[] = json_decode(json_encode($backup), true);
    }
    return new UnstructuredListData($backups);
  }

  /**
   * Retrieve the last ondemand DB backup for $alias.
   *
   * @return \Robo\ResultData
   *   The result with a message and id if successful.
   *
   * @command acquia:db-backups-last
   *
   * @param string $alias
   *   An Acquia alias.
   */
  public function acquiaDatabaseBackupsLast(string $alias)
  {
    $this->acquiaReady();
    $response = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->databaseBackups($this->acquiaAliasToUUID($alias), $this->acquiaDbName)
      ->addQuery('filter', "type=ondemand")
      ->addQuery('sort', '-created')
      ->addQuery('limit', '1')
      ->run();

    if (empty($response['result'][0])) {
      return new ResultData(ResultData::EXITCODE_ERROR, 'Unable to find any backups.', ['id' => '']);
    }
    $backup_id = $response['result'][0]->id;
    $this->output()->write($backup_id);
    $response = new ResultData(ResultData::EXITCODE_OK, sprintf('Found backup ID %s.', $backup_id), ['id' => $backup_id]);
    return $response;
  }

  /**
   * Get backup information for $alias and backup $id.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredListData
   *
   * @command acquia:db-backup-info
   *
   * @param string $alias
   *   An Acquia alias.
   * @param string $id
   *   The backup ID.
   */
  public function acquiaDatabaseBackupInfo(string $alias, string $id)
  {
    $this->acquiaReady();
    $response = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->databaseBackup($this->acquiaAliasToUUID($alias), $this->acquiaDbName, $id)
      ->run();
    return new UnstructuredListData(json_decode(json_encode($response->getArrayCopy()), true));
  }

  /**
   * Create a database backup for $alias.
   *
   * @command acquia:db-backup-create
   *
   * @param string $alias
   *   An Acquia alias.
   */
  public function acquiaDatabaseBackupCreate(string $alias) {
    $this->say(sprintf('Starting database backup of %s.', $alias));
    $this->acquiaReady();
    $result = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->createDatabaseBackup($this->acquiaAliasToUUID($alias), $this->acquiaDbName)
      ->waitForTaskCompletion($this->acquiaApplicationUuid, AcquiaTaskWatcher::DATABASE_BACKUP_CREATED, 1200, $this->acquiaWaitForTaskCompletionCallable('Database backup creation finished'))->run();
    $this->acquiaFailOnIncompleteTask($result, 'Database backup creation failed');
  }

  /**
   * Restore database backup $id for $alias.
   *
   * @command acquia:db-backup-restore
   *
   * @param string $alias
   *   An Acquia alias.
   * @param string $id
   *   The backup ID.
   */
  public function acquiaDatabaseBackupRestore(string $alias, string $id) {
    $this->say(sprintf('Starting database restoration of %s with backup %s.', $alias, $id));
    $this->acquiaReady();
    $result = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->restoreDatabaseBackup($this->acquiaAliasToUUID($alias), $this->acquiaDbName, $id)
      ->waitForTaskCompletion($this->acquiaApplicationUuid, AcquiaTaskWatcher::DATABASE_BACKUP_RESTORED, 1200, $this->acquiaWaitForTaskCompletionCallable('Database backup restoration finished'))->run();
    $this->acquiaFailOnIncompleteTask($result, 'Database backup restoration failed');
  }

  /**
   * Get the task name and status
   *
   * @param string $task_id
   *   The task ID from the initial command.
   *
   * @return null|array
   */
  protected function acquiaGetTaskStatus(string $task_id)
  {
    $this->acquiaReady();
    $response = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->tasks($this->acquiaApplicationUuid)
      ->run();
    foreach ($response['result'] as $task) {
      /* @var \AcquiaCloudApi\Response\TaskResponse $task */
      if ($task_id === $task->uuid) {
        return [$task->name, $task->status];
      }
    }
    return NULL;
  }

  /**
   * Show information about an environment.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredData
   *
   * @command acquia:environment
   *
   * @param string $alias
   *   An Acquia alias.
   */
  public function acquiaEnvironmentInfo(string $alias)
  {
    $this->acquiaReady();
    $response = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->environment($this->acquiaAliasToUUID($alias))
      ->run();
    return new UnstructuredData(json_decode(json_encode($response->getData()), true));
  }

  /**
   * Show information about an environment.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredData
   *
   * @command acquia:vcs-path
   *
   * @param string $alias
   *   An Acquia alias.
   *
   * @return \Robo\ResultData
   *   The result data in 'vcs_path'.
   */
  public function acquiaVcsPath(string $alias)
  {
    $this->acquiaReady();
    $response = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->environment($this->acquiaAliasToUUID($alias))
      ->run();
    $vcs_path = $response->getArrayCopy()['vcs']->path;
    $this->output()->write($vcs_path);
    $response = new ResultData(ResultData::EXITCODE_OK, sprintf('%s is on VCS path %s.', $alias, $vcs_path), ['vcs_path' => $vcs_path]);
    return $response;
  }

  /**
   * List out tasks.
   *
   * @command drush:status
   *
   * @param string $alias
   *   An Acquia alias.
   */
  public function drushStatus(string $alias)
  {
    $this->drush(["@$alias", 'status']);
  }

  /**
   * Run a drush command with arguments.
   *
   * @command drush
   *
   * @param array $args
   *   Pass-through arguments for drush.
   */
  public function drush(array $args)
  {
    $this->taskExec('./drush.sh')->args($args)->run();
  }


  /**
   * Import configuration on $alias environment.
   *
   * @command drush:config-import
   *
   * @param string $alias
   *   An Acquia alias.
   */
  public function drushConfigImport(string $alias)
  {
    $this->drushCacheRebuild($alias);
    // Import all config split definitions that are symlinked to their own directory.
    // This is required in rare circumstances config to enable a module is in a config
    // split but that config split has not been installed yet. This can cause it to
    // think the module should be disabled but it tries to import its config.
    $this->drush(["@$alias", 'config:import', '--partial', '--source=' . $this->acquiaGetWebRoot($alias)]);
    // Import the config twice so that new config split settings will import.
    $this->drush(["@$alias", 'config:import', '-y']);
    $this->drush(["@$alias", 'config:import', '-y']);
  }

  /**
   * Run outstanding update hooks on $alias environment.
   *
   * @command drush:updb
   *
   * @param string $alias
   *   An Acquia alias.
   */
  public function drushUpdateDatabase(string $alias) {
    $this->drush(["@$alias", 'updb', '-y']);
    $this->drushCacheRebuild($alias);
  }

  /**
   * Clear cache.
   *
   * @command drush:cache-rebuild
   *
   * @param string $alias
   *   An Acquia alias.
   */
  public function drushCacheRebuild(string $alias)
  {
    $this->drush(["@$alias", 'cache-rebuild']);
  }

  /**
   * Turn maintenance mode off or on on an environment.
   *
   * @command drush:maintenance-mode
   *
   * @param string $alias
   *   An Acquia alias.
   * @param bool $enabled
   *   True for maintenance mode or false to disable.
   */
  public function drushMaintenanceMode(string $alias, bool $enabled = true)
  {
    $this->drush(["@$alias", 'state:set', 'system.maintenance_mode', true === $enabled ? 1 : 0, '--input-format=integer']);
  }

  /**
   * Run a composer command with arguments.
   *
   * @command composer
   *
   * @param array $args
   *   Pass-through arguments for composer.
   */
  public function composer(array $args)
  {
    $this->taskExec('./composer.sh')->args($args)->run();
  }

  /**
   * Build out files for compatibility with Acquia file system.
   *
   * @command acquia:build-and-push-code
   *
   * @param string $branch
   *   The branch to push to.
   * @param string $build_number
   *   The unique ID used to create the tag (tags/$branch-$build_number).
   *
   * @return string
   *   The tag created.
   */
  public function acquiaBuildAndPushCode(string $branch, string $build_number)
  {
    // @TODO: Perhaps abstract these so others can use.
    if (!file_exists('./scripts/acquia-build-code.sh')) {
      throw new TaskExitException(static::class, './scripts/acquia-build-code.sh does not exist, you cannot build code.', Result::EXITCODE_ERROR);
    }
    if (!file_exists('./scripts/acquia-push-code.sh')) {
      throw new TaskExitException(static::class, './scripts/acquia-push-code.sh does not exist, you cannot push code.', Result::EXITCODE_ERROR);
    }
    $this->say('Building code for Acquia now...');
    $this->taskExec('./scripts/acquia-build-code.sh')->run();
    $this->say('Finished building code for Acquia');
    $this->say('Pushing code to Acquia now...');
    $this->taskExec('./scripts/acquia-push-code.sh')->args([$this->acquiaGitURL, $branch, $build_number])->run();
    $this->say("Pushed code to branch $branch");
    $tag = $this->acquiaCreateVcsPath($branch, $build_number);
    $this->say("Pushed code to tag $tag");
    $this->say('Finished pushing code to Acquia');
    return $tag;
  }

  /**
   * Download the Acquia aliases to the current directory.
   *
   * @command acquia:aliases
   */
  public function acquiaAliases()
  {
    $this->acquiaReady();
    $this->say('Downloading Acquia Drush Aliases');
    $result = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->drushAliases()
      ->addQuery('version ', 9)
      ->run();
    $file_name = 'acquia-cloud.drush-9-aliases.tar.gz';
    $this->taskWriteToFile($file_name)->text((string) $result->getArrayCopy()['file'])->run();
    $this->say(sprintf('Your Acquia aliases have been downloaded to %s', realpath($file_name)));
  }

  /**
   * Switch code on $alias to tag $tag.
   *
   * @command acquia:switch-code
   *
   * @param string $alias
   *   An Acquia alias.
   * @param string $vcs_path
   *   A tag name (ie, tags/release-1234) or branch (ie, release).
   */
  public function acquiaSwitchCode(string $alias, string $vcs_path)
  {
    $this->acquiaReady();
    $this->say(sprintf('Code switch started on %s to %s', $alias, $vcs_path));
    $result = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->switchCode($this->acquiaAliasToUUID($alias), $vcs_path)
      ->waitForTaskCompletion($this->acquiaApplicationUuid, AcquiaTaskWatcher::CODE_SWITCHED, 1200, $this->acquiaWaitForTaskCompletionCallable('Code switch finished'))->run();
    $this->acquiaFailOnIncompleteTask($result, 'Copy code failed');
  }

  /**
   * Copy code to $alias_to from $alias_from.
   *
   * @command acquia:copy-code
   *
   * @param string $alias_to
   *   Copy code to Acquia alias.
   * @param string|null $alias_from
   *   Copy code from Acquia alias (Default prod).
   */
  public function acquiaCopyCode(string $alias_to, string $alias_from = null)
  {
    $this->acquiaReady();
    $alias_from = $alias_from ?: $this->acquiaProdEnv;
    $this->say(sprintf('Code copy started to %s to %s', $alias_to, $alias_from));
    $result = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->deployCode($this->acquiaAliasToUUID($alias_from), $this->acquiaAliasToUUID($alias_to))
      ->waitForTaskCompletion($this->acquiaApplicationUuid, self::TASK_CODE_CODE_DEPLOYED, 1200, $this->acquiaWaitForTaskCompletionCallable('Copy code finished'))->run();
    $this->acquiaFailOnIncompleteTask($result, 'Copy code failed');
  }

  /**
   * Copy files to $alias_to from $alias_from.
   *
   * @command acquia:copy-files
   *
   * @param string $alias_to
   *   Copy files to Acquia alias.
   * @param string|null $alias_from
   *   Copy files from Acquia alias (Default prod).
   */
  public function acquiaCopyFiles(string $alias_to, string $alias_from = null)
  {
    $this->acquiaReady();
    $alias_from = $alias_from ?: $this->acquiaProdEnv;
    if ($this->acquiaProdEnv === $alias_to) {
      throw new TaskExitException(static::class, 'You may not copy files to production', Result::EXITCODE_ERROR);
    }
    $this->say(sprintf('Copy files started to %s from %s', $alias_to, $alias_from));
    $result = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->copyFiles($this->acquiaAliasToUUID($alias_from), $this->acquiaAliasToUUID($alias_to))
      ->waitForTaskCompletion($this->acquiaApplicationUuid, self::TASK_CODE_FILES_COPIED, 1200, $this->acquiaWaitForTaskCompletionCallable('Copy files finished'))->run();
    $this->acquiaFailOnIncompleteTask($result, 'Copying files failed');
  }

  /**
   * Copy the database to $alias_to from $alias_from.
   *
   * @command acquia:copy-db
   *
   * @param string $alias_to
   *   Copy DB to Acquia alias.
   * @param string|null $alias_from
   *   Copy DB from Acquia alias (Default prod).
   */
  public function acquiaCopyDatabase(string $alias_to, string $alias_from = null)
  {
    $this->acquiaReady();
    $alias_from = $alias_from ?: $this->acquiaProdEnv;
    if ($this->acquiaProdEnv === $alias_to) {
      throw new TaskExitException(static::class, 'You may not copy DB to production', Result::EXITCODE_ERROR);
    }
    $this->say(sprintf('Copy DB started to %s from %s', $alias_to, $alias_from));
    $result = $this->taskAcquiaCloudApiStack($this->acquiaKey, $this->acquiaSecret)
      ->databaseCopy($this->acquiaAliasToUUID($alias_from), $this->acquiaDbName, $this->acquiaAliasToUUID($alias_to))
      ->waitForTaskCompletion($this->acquiaApplicationUuid, AcquiaTaskWatcher::DATABASE_COPIED, 1200, $this->acquiaWaitForTaskCompletionCallable('Copy DB finished'))->run();
    $this->acquiaFailOnIncompleteTask($result, 'Copying DB failed');
  }

  /**
   * With a task that returns a task ID, determine if it completed.
   *
   * @param \Robo\Result $result
   *   The result of an API call that returns a task ID.
   * @param string $message_on_error
   *   A message to show if the task was not successfully completed.
   *
   * @return bool
   *   True if the API call was successful an exception if not.
   */
  protected function acquiaFailOnIncompleteTask(Result $result, $message_on_error = 'Task did not complete successfully')
  {
    if ($result->getExitCode() === Result::EXITCODE_OK) {
      $task_id = $result->getArrayCopy()['result']->getMessage();
      $this->say(sprintf('Ensuring that the previous task (%s), now finished, was also successful.', $task_id));
      if (!empty($task_id) && NULL !== $task_status = $this->acquiaGetTaskStatus($task_id)) {
        if ('completed' === $task_status[1]) {
          return TRUE;
        }
        throw new TaskExitException(static::class, $message_on_error . sprintf('. The task status was %s.', $task_status[1]), Result::EXITCODE_ERROR);
      }
      $this->say(sprintf('The command did not return a task ID on completion, unable to determine success. Please see command result %s', print_r($result->getArrayCopy(), 1)));
      return TRUE;
    }
    throw new TaskExitException(static::class, $message_on_error, Result::EXITCODE_ERROR);
  }

  /**
   * Get the web root on an Acquia environment.
   *
   * @param string $alias
   *   An Acquia alias.
   *
   * @return string
   */
  protected function acquiaGetWebRoot(string $alias)
  {
    return "/var/www/html/{$this->acquiaSubName}.$alias/config/config_splits";
  }

  /**
   * Build a new tag and deploy it on $alias.
   *
   * @command acquia:deploy-new-build
   *
   * @param string $aliases_to
   *   A comma separated list of environment aliases to update.
   * @param string $branch
   *   The branch to push to.
   * @param string $build_number
   *   The unique ID used to create the tag (tags/$branch-$build_number).
   */
  public function acquiaDeployNewBuild(string $aliases_to, string $branch, string $build_number)
  {
    $this->acquiaReady();
    $vcs_path = $this->acquiaBuildAndPushCode($branch, $build_number);
    $this->acquiaDeployVcsPath($aliases_to, $vcs_path);
  }

  /**
   * Deploy a copy of the code, db, and/or files to 1 or more diff envs.
   *
   * @param string $type
   *   Either vcs_path or env.
   * @param string $aliases_to
   *   A comma separated list of environment aliases to update.
   * @param string $code
   *   If $type is vcs_path, the branch or tag, if env an alias.
   * @param string $alias_from_db
   *   The environment to copy the DB from, 0 to skip.
   * @param string $alias_from_files
   *   The environment to copy the files from, 0 to skip.
   * @param string $backup_first
   *   Create a backup of the database before starting the deployment.
   */
  protected function acquiaDeploy(string $type, string $aliases_to, string $code, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
  {
    $this->applyToAllAliases($aliases_to, function($alias_to) use ($type, $code, $alias_from_db, $alias_from_files, $backup_first) {
      if ('1' === $backup_first) {
        $this->acquiaDatabaseBackupCreate($alias_to);
        $result = $this->acquiaDatabaseBackupsLast($alias_to);
        if ($result->getExitCode() === Result::EXITCODE_OK) {
          $this->say(sprintf('A database backup with ID %s has been created on environment %s. Restore using the command acquia:db-backup-restore %s %s', $result->getArrayCopy()['id'], $alias_to, $alias_to, $result->getArrayCopy()['id']));
        }
        else {
          throw new TaskExitException(
            static::class,
            'Unable to create a database backup for deployment, exiting.',
            Result::EXITCODE_ERROR
          );
        }
      }
      $this->drushMaintenanceMode($alias_to, TRUE);
      // Get the current VCS path of the environment before it is switched.
      $result = $this->acquiaVcsPath($alias_to);
      if ($result->getExitCode() === Result::EXITCODE_OK) {
        $current_vcs_path = $result['vcs_path'];
      }
      else {
        throw new TaskExitException(
          static::class,
          'Unable to determine current code before switching.',
          Result::EXITCODE_ERROR
        );
      }
      switch ($type) {
        // $code is a drush alias to copy code from.
        case 'env':
          if ($alias_to !== $code) {
            $this->say(sprintf('%s is currently on %s before switching to %s\'s code.', $alias_to, $current_vcs_path, $code));
            $this->acquiaCopyCode($alias_to, $code);
          }
          break;
        // $code is a branch or tag to switch to.
        case 'vcs_path':
          $this->say(sprintf('%s is currently on %s before switching to %s.', $alias_to, $current_vcs_path, $code));
          $this->acquiaSwitchCode($alias_to, $code);
          break;

        default:
          throw new TaskExitException(
            static::class,
            '$type must be either env or vcs_path.',
            Result::EXITCODE_ERROR
          );

      }
      if ('0' !== $alias_from_db && $alias_to !== $alias_from_db) {
        $this->acquiaCopyDatabase($alias_to, $alias_from_db);
      }
      if ('0' !== $alias_from_files && $alias_to !== $alias_from_files) {
        $this->acquiaCopyFiles($alias_to, $alias_from_db);
      }
      $this->drushConfigImport($alias_to);
      $this->drushUpdateDatabase($alias_to);
      $this->drushMaintenanceMode($alias_to, FALSE);
    });
  }

  /**
   * Deploy a copy of the code, db, and/or files to 1 or more diff envs.
   *
   * @command acquia:deploy-branch-or-tag
   *
   * @param string $aliases_to
   *   A comma separated list of environment aliases to update.
   * @param string $vcs_path
   *   A tag name (ie, tags/release-1234) or branch (ie, release).
   * @param string $alias_from_db
   *   The environment to copy the DB from, 0 to skip.
   * @param string $alias_from_files
   *   The environment to copy the files from, 0 to skip.
   * @param string $backup_first
   *   Create a backup of the database before starting the deployment.
   */
  public function acquiaDeployVcsPath(string $aliases_to, string $vcs_path, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
  {
    $this->acquiaReady();
    call_user_func_array([$this, 'acquiaDeploy'], array_merge(['vcs_path'], func_get_args()));
  }

  /**
   * Deploy a copy of the code, db, and/or files to 1 or more diff envs.
   *
   * @command acquia:deploy-copy
   *
   * @param string $aliases_to
   *   A comma separated list of environment aliases to update.
   * @param string $alias_from_code
   *   The environment to copy the code from.
   * @param string $alias_from_db
   *   The environment to copy the DB from, 0 to skip.
   * @param string $alias_from_files
   *   The environment to copy the files from, 0 to skip.
   * @param string $backup_first
   *   Create a backup of the database before starting the deployment.
   */
  public function acquiaDeployCopy(string $aliases_to, string $alias_from_code, string $alias_from_db = '0', string $alias_from_files = '0', string $backup_first = '0')
  {
    $this->acquiaReady();
    call_user_func_array([$this, 'acquiaDeploy'], array_merge(['env'], func_get_args()));
  }


  /**
   * Refresh the $aliases_to to copies of production.
   *
   * @command acquia:deploy-refresh-from-prod
   *
   * @param string $aliases_to
   *   A comma separated list of environment aliases to update.
   * @param string $backup_first
   *   Create a backup of the database before starting the deployment.
   *
   * @throws \Robo\Exception\TaskExitException
   */
  public function acquiaDeployRefreshFromProd(string $aliases_to, string $backup_first = '0')
  {
    $this->acquiaReady();
    $this->acquiaDeploy('env', $aliases_to, $this->acquiaProdEnv, $this->acquiaProdEnv, $this->acquiaProdEnv, $backup_first);
  }

  /**
   * Run the $function on each alias in $aliases_to.
   *
   * @param string $aliases_to
   *   A comma separated list of environment aliases to update.
   * @param callable $function
   *   A callback to apply to each alias.
   */
  protected function applyToAllAliases(string $aliases_to, callable $function)
  {
    $aliases_to = array_filter(explode(',', $aliases_to));
    foreach ($aliases_to as $alias_to) {
      $function($alias_to);
    }
  }

  /**
   * Create a VCS Path from a branch + build.
   *
   * @param string $branch
   *   The branch this tag was was built from.
   * @param string $build_number
   *   A unique ID.
   *
   * @return string
   *   A full tag name (tags/*).
   */
  protected function acquiaCreateVcsPath($branch, $build_number) {
    return sprintf('tags/%s-%s', $branch, $build_number);
  }

  /**
   * Create a callable for waitForTaskCompletion().
   *
   * @param string $completion_message
   *   Message when finishes.
   * @param false $add_confirm_to_continue
   *   Add a confirm if you want to continue dialog.
   *
   * @return \Closure
   */
  protected function acquiaWaitForTaskCompletionCallable(string $completion_message, bool $add_confirm_to_continue = FALSE) {
    return function ($result) use ($completion_message, $add_confirm_to_continue)
    {
      static $i = 0;
      if ($i === 0) {
        $this->output()->write("\nWaiting for task completion.");
      }
      $i++;
      // Print a dot every 3 seconds.
      if ($i % 3 === 0) {
        $this->output()->write('.');
      }
      // If result is empty, that means the job is complete.
      if (empty($result[0]->status)) {
        $this->say("\n$completion_message");
        if (true === $add_confirm_to_continue && !$this->confirm('Would you like to continue?')) {
          throw new TaskExitException(
            static::class,
            'Cancelled.',
            Result::EXITCODE_USER_CANCEL
          );
        }
      }
    };
  }

}
